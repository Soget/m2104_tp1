﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WinFormsApp;

namespace TestUnitCompteur
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Testincrementation()
        {
            Assert.AreEqual(1, nombre.incrementation());
        }
        [TestMethod]
        public void TestDecrementation()
        {
            nombre = 1;
            Assert.AreEqual(0, nombre.decrementation());
        }
        [TestMethod]
        public void TestReset()
        {
            nombre = 1;
            Assert.AreEqual(0, nombre.reset());
        }
    }
}
