﻿Imports System
Imports WinFormsApp

Public Class Form1
    Dim Compt = New Compteur()
    Private Sub Btn_reset_Click(sender As Object, e As EventArgs) Handles Btn_reset.Click
        Compt.Reset()
        lbl_nombre.Text = Compt.Afficher()
    End Sub

    Private Sub Btn_moins_Click(sender As Object, e As EventArgs) Handles Btn_moins.Click
        Compt.Decrementation()
        lbl_nombre.Text = Compt.Afficher()
    End Sub

    Private Sub Btn_plus_Click(sender As Object, e As EventArgs) Handles Btn_plus.Click
        Compt.Incrementation()
        lbl_nombre.Text = Compt.Afficher()
    End Sub
End Class
