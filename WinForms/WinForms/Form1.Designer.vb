﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Btn_plus = New System.Windows.Forms.Button()
        Me.Btn_moins = New System.Windows.Forms.Button()
        Me.Btn_reset = New System.Windows.Forms.Button()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.lbl_nombre = New System.Windows.Forms.Label()
        Me.Total = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Btn_plus
        '
        Me.Btn_plus.Location = New System.Drawing.Point(1050, 390)
        Me.Btn_plus.Margin = New System.Windows.Forms.Padding(6)
        Me.Btn_plus.Name = "Btn_plus"
        Me.Btn_plus.Size = New System.Drawing.Size(150, 44)
        Me.Btn_plus.TabIndex = 0
        Me.Btn_plus.Text = "+"
        Me.Btn_plus.UseVisualStyleBackColor = True
        '
        'Btn_moins
        '
        Me.Btn_moins.Location = New System.Drawing.Point(448, 390)
        Me.Btn_moins.Margin = New System.Windows.Forms.Padding(6)
        Me.Btn_moins.Name = "Btn_moins"
        Me.Btn_moins.Size = New System.Drawing.Size(150, 44)
        Me.Btn_moins.TabIndex = 1
        Me.Btn_moins.Text = "-"
        Me.Btn_moins.UseVisualStyleBackColor = True
        '
        'Btn_reset
        '
        Me.Btn_reset.Location = New System.Drawing.Point(748, 531)
        Me.Btn_reset.Margin = New System.Windows.Forms.Padding(6)
        Me.Btn_reset.Name = "Btn_reset"
        Me.Btn_reset.Size = New System.Drawing.Size(150, 44)
        Me.Btn_reset.TabIndex = 2
        Me.Btn_reset.Text = "RAZ"
        Me.Btn_reset.UseVisualStyleBackColor = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'lbl_nombre
        '
        Me.lbl_nombre.AutoSize = True
        Me.lbl_nombre.Location = New System.Drawing.Point(804, 390)
        Me.lbl_nombre.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lbl_nombre.Name = "lbl_nombre"
        Me.lbl_nombre.Size = New System.Drawing.Size(24, 26)
        Me.lbl_nombre.TabIndex = 4
        Me.lbl_nombre.Text = "0"
        '
        'Total
        '
        Me.Total.AutoSize = True
        Me.Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Total.Location = New System.Drawing.Point(796, 198)
        Me.Total.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Total.Name = "Total"
        Me.Total.Size = New System.Drawing.Size(44, 20)
        Me.Total.TabIndex = 5
        Me.Total.Text = "Total"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1600, 865)
        Me.Controls.Add(Me.Total)
        Me.Controls.Add(Me.lbl_nombre)
        Me.Controls.Add(Me.Btn_reset)
        Me.Controls.Add(Me.Btn_moins)
        Me.Controls.Add(Me.Btn_plus)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Btn_plus As Button
    Friend WithEvents Btn_moins As Button
    Friend WithEvents Btn_reset As Button
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents lbl_nombre As Label
    Friend WithEvents Total As Label
End Class
